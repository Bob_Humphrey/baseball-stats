<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
//use App\Http\Requests;
use App\Http\Controllers\Controller;

class NavBarFormController extends Controller
{

    public function standings(Request $request)
    {
        $year = $request->input('year');
        $player = $request->input('player');
        if ($year) {
            if (!preg_match('/^(18|19|20)[0-9]{2}$/s', $year)) {
                flashMessage("$year is not a valid year.", "alert-danger");
                return back();
            } elseif ($year < 1871) {
                flashMessage("Year cannot be earlier than 1871.", "alert-danger");
                return back();
            } elseif ($year > 2012) {
                flashMessage("Year cannot be later than 2012.", "alert-danger");
                return back();
            } else {
                return redirect('teams/standings/' . $year);
            }
        } elseif ($player) {
            return redirect('players/list/' . $player);
        } else {
            flashMessage("You must enter either the standings year or the beginning of a player's last name.", "alert-danger");
            return back();
        }
    }
}
