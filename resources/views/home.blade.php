@extends('app')

@section('pageTitle')

Baseball Stats

@stop

@section('pageClass') home-page @stop

@section('content')

<div>
    <h3>Description</h3>
    <p>
      <b>Baseball Stats</b> displays the final standings and statistics for the
      Major Leagues from 1871 to 2012.  Every player is included, from Hank
      Aaron to Dutch Zwilling, and all 120 franchises are here, from the
      immortal New York Yankees to the nearly forgotten Brooklyn Tip-Tops.
      You can find out what year Ted Williams batted .406 while hitting 37
      home runs (1941), or check out what year the Montreal Expos became the
      Washington Nationals (2005).
    </p>
    <h3>Built With</h3>
    <p>
      PHP, Laravel, MySQL, and Bootstrap.
      The data for the application is from
      <a href="http://www.seanlahman.com/baseball-archive/statistics/"
      target="blank">Sean Lahman’s Baseball Database</a>, which is
      copyright by Sean Lahman and available for use under a Creative
      Commons Attribution-ShareAlike 3.0 License.
    </p>
    <h3>To Use</h3>
    <p>
      Start by entering a query in the navigation bar at the top of
      the page.  Enter a year to see the standings for that year.  Or
      key in the last name of any player to see that player’s statistics
      for every year of his career.  You don’t have to type the player’s
      entire name.  Just enter one letter or more to find all the players
      that begin with those letters.  You can also click the FRANCHISES link
      to see a list of all of the Major League franchises.
    </p>

  </div>
</div>


@stop
