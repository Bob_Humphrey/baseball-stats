<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Baseball Statistics</title>
  <link rel="stylesheet" href="/css/all.css">
  <link rel="icon" type="image/x-icon" href="/favicon.ico" />
</head>
<body>
  @include('partials.navbar')
  <section id="content" class="container">
    <div id="page-heading">
      <h2>@yield('pageTitle')</h2>
    </div>
    <div class=" @yield('pageClass')">
      @include('partials.alerts')
      @yield('content')
    </div>
  </section>
  @include('partials.footer')
  <script src="/js/all.js"></script>
</body>
</html>
