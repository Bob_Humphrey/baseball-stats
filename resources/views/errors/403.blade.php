@extends('app')

@section('pageTitle')

Sorry!

@stop

@section('pageClass') error-page @stop

@section('content')

<div class="error-message">
  You are not allowed to view this page.
</div>

@stop
