@extends('app')

@section('pageTitle')

Sorry!

@stop

@section('pageClass') error-page @stop

@section('content')

<div class="error-message">
  We were unable to find the page you requested.
</div>

@stop
